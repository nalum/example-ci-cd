.PHONY: bench-all
bench-all: ##@bench Run all benchmark tests
bench-all: bench-api-1 bench-api-2

.PHONY: bench-api-1
bench-api-1: ##@bench Run the benchmarks for the API 1 Service
	@echo "Running benchmark tests for the API 1 Service"
	@go test -bench . ./cmd/service/api-1
	@echo "API 1 Service benchmark tests complete"

.PHONY: bench-api-2
bench-api-2: ##@bench Run the benchmarks for the API 2 Service
	@echo "Running benchmark tests for the API 2 Service"
	@go test -bench . ./cmd/service/api-2
	@echo "API 2 Service benchmark tests complete"
