.PHONY: docker-all
docker-all: docker-api-1 docker-api-2

.PHONY: docker-api-1
docker-api-1:
	@echo "Building API 1 Service Docker Image"
	@docker build -f ./docker/api-1.Dockerfile -t ${CI_REGISTRY_IMAGE}/api-1:${CI_BUILD_TAG} --build-arg APP_NAME="bin/service-${CI_PROJECT_NAME}-api-1-${CI_BUILD_TAG}" --build-arg VERSION="${CI_BUILD_TAG}" --build-arg COMMIT="${CI_BUILD_REF}" .
	@docker push ${CI_REGISTRY_IMAGE}/api-1:${CI_BUILD_TAG}
	@echo "API 1 Service Docker Image built and located at ${CI_REGISTRY_IMAGE}/api-1:${CI_BUILD_TAG}"


.PHONY: docker-api-2
docker-api-2:
	@echo "Building API 2 Service Docker Image"
	@docker build -f ./docker/api-2.Dockerfile -t ${CI_REGISTRY_IMAGE}/api-2:${CI_BUILD_TAG} --build-arg APP_NAME="bin/service-${CI_PROJECT_NAME}-api-2-${CI_BUILD_TAG}" --build-arg VERSION="${CI_BUILD_TAG}" --build-arg COMMIT="${CI_BUILD_REF}" .
	@docker push ${CI_REGISTRY_IMAGE}/api-2:${CI_BUILD_TAG}
	@echo "API 2 Service Docker Image built and located at ${CI_REGISTRY_IMAGE}/api-2:${CI_BUILD_TAG}"
