.PHONY: test-all
test-all: test-api-1 test-api-2

.PHONY: test-api-1
test-api-1:
	@echo "Testing API 1 Service"
	@go test ./cmd/service/api-1
	@echo "API 1 Service tests completed"

.PHONY: test-api-2
test-api-2:
	@echo "Testing API 2 Service"
	@go test ./cmd/service/api-2
	@echo "API 2 Service tests completed"
