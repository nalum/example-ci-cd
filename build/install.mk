.PHONY: install-all
install-all: install-api-1 install-api-2

.PHONY: install-api-1
install-api-1:
	@echo "Installing API 1 Service"
	@go install ./cmd/service/api-1
	@echo "API 1 Service installed to ${GOPATH}/bin/api-1"

.PHONY: install-api-2
install-api-2:
	@echo "Installing API 2 Service"
	@go install ./cmd/service/api-2
	@echo "API 2 Service installed to ${GOPATH}/bin/api-2"
