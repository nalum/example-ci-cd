# TODO: implement version checking to not allow deployments of older versions
.PHONY: deploy-all
deploy-all: deploy-api-1 deploy-api-2

.PHONY: deploy-pre
deploy-pre:
	@echo "Creating Deployment for the Service ${TARGET} on ${ENVIRONMENT}"
	@cp kubernetes/Deployment/deployment.yaml kubernetes/Deployment/${TARGET}.yaml
	@cp kubernetes/Service/service.yaml kubernetes/Service/${TARGET}.yaml
	@sed -i 's/{TARGET}/'${TARGET}'/' kubernetes/Deployment/${TARGET}.yaml
	@sed -i 's/{MAJOR_VERSION}/'${MAJOR_VERSION}'/' kubernetes/Deployment/${TARGET}.yaml
	@sed -i 's/{CI_PROJECT_NAME}/'${CI_PROJECT_NAME}'/' kubernetes/Deployment/${TARGET}.yaml
	@sed -i 's@{CI_REGISTRY_IMAGE}@'${CI_REGISTRY_IMAGE}'@' kubernetes/Deployment/${TARGET}.yaml
	@sed -i 's/{CI_BUILD_TAG}/'${CI_BUILD_TAG}'/' kubernetes/Deployment/${TARGET}.yaml
	@sed -i 's/{REPLICAS}/'${REPLICAS}'/' kubernetes/Deployment/${TARGET}.yaml
	@cat kubernetes/Deployment/${TARGET}.yaml
	@sed -i 's/{TARGET}/'${TARGET}'/' kubernetes/Service/${TARGET}.yaml
	@sed -i 's/{MAJOR_VERSION}/'${MAJOR_VERSION}'/' kubernetes/Service/${TARGET}.yaml
	@sed -i 's/{CI_PROJECT_NAME}/'${CI_PROJECT_NAME}'/' kubernetes/Service/${TARGET}.yaml
	@cat kubernetes/Service/${TARGET}.yaml

.PHONY: deploy-api-1
deploy-api-1: deploy-pre
	@echo "Deploying the API 1 Service"
	kubectl --record --namespace ${KUBE_NAMESPACE} apply -f kubernetes/Deployment/${TARGET}.yaml
	kubectl --record --namespace ${KUBE_NAMESPACE} apply -f kubernetes/Service/${TARGET}.yaml
	@echo "Analytics Service deployed"

.PHONY: deploy-api-2
deploy-api-2: deploy-pre
	@echo "Deploying the API 2 Service"
	kubectl --record --namespace ${KUBE_NAMESPACE} apply -f kubernetes/Deployment/${TARGET}.yaml
	kubectl --record --namespace ${KUBE_NAMESPACE} apply -f kubernetes/Service/${TARGET}.yaml
	@echo "Analytics Service deployed"
