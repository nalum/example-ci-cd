.PHONY: build-all
build-all: build-api-1 build-api-2

.PHONY: build-api-1
build-api-1:
	@echo "Compiling the API 1 Service"
	@go build -o ${CI_PROJECT_DIR}/bin/service-${CI_PROJECT_NAME}-api-1-${CI_BUILD_REF_NAME} ./cmd/service/api-1
	@echo "API 1 Service compiled: ${CI_PROJECT_DIR}/bin/service-${CI_PROJECT_NAME}-api-1-${CI_BUILD_REF_NAME}"

.PHONY: build-api-2
build-api-2:
	@echo "Compiling the API 2 Service"
	@go build -o ${CI_PROJECT_DIR}/bin/service-${CI_PROJECT_NAME}-api-2-${CI_BUILD_REF_NAME} ./cmd/service/api-2
	@echo "API 2 Service compiled: ${CI_PROJECT_DIR}/bin/service-${CI_PROJECT_NAME}-api-2-${CI_BUILD_REF_NAME}"
