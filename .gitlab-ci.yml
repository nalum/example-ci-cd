---

image: golang:latest

stages:
- cache
- build
- test
- container
- dragons
- code-review
- staging
- release-candidate
- production

##########
## The following block of yaml defines templates that can be used in jobs for the
## GitLab CI.
##
## Note: If you want to jump down to the jobs search for JOBS START HERE
##########

## This block is the base for the build jobs
.build-definition: &build-definition
  tags:
  - docker
  stage: build
  cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
    - vendor
    policy: pull
  before_script:
  # install ssh-agent
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  # run ssh-agent
  - eval $(ssh-agent -s)
  # disable host key checking (NOTE: makes you susceptible to man-in-the-middle attacks)
  # WARNING: use only in docker container, if you use it with shell you will overwrite your user's ssh config
  - mkdir -p ~/.ssh
  - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  # Install go-bindata
  - go get -u github.com/jteeuwen/go-bindata/...
  # Setup project under $GOPATH
  - mkdir -p $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}
  - cd $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}

## This block should be used when you do not need artifacts
.build-non-version: &build-non-version
  <<: *build-definition
  except:
  - /^v((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(alpha|beta|rc)\.(0|[1-9]\d*))?)$/

## This block should be used when you need artifacts
.build-version: &build-version
  <<: *build-definition
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
    - ./bin/service-*
  only:
  - /^v((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(alpha|beta|rc)\.(0|[1-9]\d*))?)$/
  except:
  - branches
  - triggers

## This block is the base for the test jobs
.test-definition: &test-definition
  tags:
  - docker
  stage: test
  cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
    - vendor
    policy: pull
  before_script:
  # install ssh-agent
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  # run ssh-agent
  - eval $(ssh-agent -s)
  # disable host key checking (NOTE: makes you susceptible to man-in-the-middle attacks)
  # WARNING: use only in docker container, if you use it with shell you will overwrite your user's ssh config
  - mkdir -p ~/.ssh
  - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  # Install go-bindata
  - go get -u github.com/jteeuwen/go-bindata/...
  # Setup project under $GOPATH
  - mkdir -p $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}
  - cd $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}
  script:
  - make test-$TARGET

## This block is the base for the container jobs
.container-definition: &container-definition
  tags:
  - docker
  stage: container
  image: docker:1.12-git
  services:
  - docker:1.12-dind
  except:
  - branches
  - triggers
  script:
  - apk add --no-cache make
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
  - make docker-$TARGET
  only:
  - /^v((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(alpha|beta|rc)\.(0|[1-9]\d*))?)$/

## This block is the base for all deployments
.deployment-definition: &deployment-definition
  dependencies: []
  image: # Docker image with kubectl setup
  except:
  - branches
  - triggers
  before_script:
  - apk add --no-cache make

## This block deploys to dragons gen
.deployment-dragons: &deployment-dragons
  <<: *deployment-definition
  environment:
    name: general/dragons
    url: https://api.dragons.mallon.io
  stage: dragons
  tags:
  - kubernetes
  only:
  - /^v((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(alpha|beta|rc)\.(0|[1-9]\d*))?)$/

## This block deploys to staging gen
.deployment-staging: &deployment-staging
  <<: *deployment-definition
  environment:
    name: general/staging
    url: https://api.staging.mallon.io
  stage: staging
  tags:
  - kubernetes
  when: manual
  only:
  - /^v((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(alpha|beta|rc)\.(0|[1-9]\d*))?)$/

## This block deploys to production gen
.deployment-production: &deployment-production
  <<: *deployment-definition
  environment:
    name: general/production
    url: https://api.mallon.io
  stage: production
  tags:
  - kubernetes
  when: manual
  only:
  - /^v((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(alpha|beta|rc)\.(0|[1-9]\d*))?)$/

##########
## JOBS START HERE
##########

##########
## Start build jobs
##########
cache-vendor:
  stage: cache
  cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
    - vendor
    policy: push
  tags:
  - docker
  script:
  # install ssh-agent
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  # run ssh-agent
  - eval $(ssh-agent -s)
  # disable host key checking (NOTE: makes you susceptible to man-in-the-middle attacks)
  # WARNING: use only in docker container, if you use it with shell you will overwrite your user's ssh config
  - mkdir -p ~/.ssh
  - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  # Install glide dep manager
  - go get github.com/Masterminds/glide/...
  # Setup project under $GOPATH
  - mkdir -p $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}
  - cd $GOPATH/src/gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}
  # Install app dependencies
  - glide i -v

codeclimate:
  stage: code-review
  tags:
  - docker
  image: docker:latest
  except:
  - tags
  variables:
    DOCKER_HOST: 127.0.0.1:2375
    privileged: "true"
  services:
    - docker:dind
  script:
    - docker pull codeclimate/codeclimate
    - docker run --env CODECLIMATE_CODE="$PWD" --volume "$PWD":/code --volume /var/run/docker.sock:/var/run/docker.sock --volume /tmp/cc:/tmp/cc codeclimate/codeclimate engines:install
    - docker run --env CODECLIMATE_CODE="$PWD" --volume "$PWD":/code --volume /var/run/docker.sock:/var/run/docker.sock --volume /tmp/cc:/tmp/cc codeclimate/codeclimate analyze -f json > codeclimate.json
  artifacts:
    paths: [codeclimate.json]

compile:
  <<: *build-non-version
  script:
  - export TARGET=api-1
  - make build-$TARGET
  - export TARGET=api-2
  - make build-$TARGET

compile:version:
  <<: *build-version
  script:
  - export TARGET=api-1
  - make build-$TARGET
  - export TARGET=api-2
  - make build-$TARGET

##########
## End build jobs
##########

##########
## Start test jobs
##########
test:api-1:
  <<: *test-definition
  variables:
    TARGET: api-1

test:api-2:
  <<: *test-definition
  variables:
    TARGET: api-2

##########
## End test jobs
##########

##########
## Start container jobs
##
## Make sure to add the correct dependencies to each job to limit the artifacts
## that the job fetches.
##
## The below jobs will build Docker containers for tags that match the following
## regex: /^v((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(alpha|beta|rc)\.(0|[1-9]\d*))?)$/
##
## examples:
## - v0.1.0-alpha.0    <-- This gets built
## - v0.1.0-alpha.0-f  <-- This does not get built
## - v0.1.0-alpha      <-- This does not get built
## - v0.1.0-alpha0     <-- This does not get built
##########
api-1:
  <<: *container-definition
  dependencies:
  - compile:version
  variables:
    TARGET: api-1
    DOCKER_HOST: 127.0.0.1:2375
    privileged: "true"
  
api-2:
  <<: *container-definition
  dependencies:
  - compile:version
  variables:
    TARGET: api-2
    DOCKER_HOST: 127.0.0.1:2375
    privileged: "true"

##########
## End container jobs
##########

##########
## Start deployment jobs
##
## The below jobs will push a new version of the services to the kubernetes cluster.
## These jobs are only run when a match is found against the following regex:
## /^v((0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(alpha|beta|rc)\.(0|[1-9]\d*))?)$/
##
## The job for dragons and staging are run automatically but the rc/production jobs
## requires manual firing.
##
## examples:
## - v0.1.0-alpha.0    <-- This gets built
## - v0.1.0-alpha.0-f  <-- This does not get built
## - v0.1.0-alpha      <-- This does not get built
## - v0.1.0-alpha0     <-- This does not get built
##########
dragons:GEN:
  <<: *deployment-dragons
  variables:
    KUBE_NAMESPACE: dragons
    ENVIRONMENT: dragons
  script:
  - export TARGET=api-1
  - make deploy-$TARGET
  - export TARGET=api-2
  - make deploy-$TARGET

staging:GEN:
  <<: *deployment-staging
  variables:
    KUBE_NAMESPACE: staging
    ENVIRONMENT: staging
  script:
  - export TARGET=api-1
  - make deploy-$TARGET
  - export TARGET=api-2
  - make deploy-$TARGET

production:GEN:
  <<: *deployment-production
  variables:
    KUBE_NAMESPACE: production
    ENVIRONMENT: production
    REPLICAS: "4"
  script:
  - export TARGET=api-1
  - make deploy-$TARGET
  - export TARGET=api-2
  - make deploy-$TARGET

##########
## End deployment jobs
##########
