package main

import (
	"testing"

	"github.com/gin-gonic/gin"
)

const (
	GinKeyTestAPI2 = "test-api-2"
)

func init() {
	//Set Gin to Test Mode
	gin.SetMode(gin.TestMode)
}

func TestServiceName(t *testing.T) {
	if serviceName != "API 2" {
		t.Error("serviceName is not API 2.")
	}
}
