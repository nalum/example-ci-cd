package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
)

func router() *gin.Engine {
	engine := gin.New()

	// Set metrics route
	// NOTE: adding before the logger as we don't need to log requests to this endpoint
	engine.GET("/metrics", gin.WrapH(prometheus.Handler()))

	// Liveness and Readyness endpoints for kubernetes deployments
	engine.GET("/alive", alive)
	engine.GET("/ready", ready)

	// Adding the default gin middleware after the alive and ready endpoints so as not to polute the
	// logs with requests to these two endpoints
	engine.Use(
		gin.Logger(),
		gin.Recovery(),
	)

	api2 := engine.Group("/api-1")
	{
		api2.GET("/:identifier", getResponse)
	}

	return engine
}

func alive(c *gin.Context) {
	c.Data(http.StatusOK, "text/plain", []byte(`OK`))
}

func ready(c *gin.Context) {
	c.Data(http.StatusOK, "text/plain", []byte(`OK`))
}
