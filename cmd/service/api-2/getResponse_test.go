package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAPI1Identifier(t *testing.T) {
	req := httptest.NewRequest("GET", "https://api.mallon.io/api-2/test-request", nil)
	w := httptest.NewRecorder()

	engine := router()
	engine.ServeHTTP(w, req)

	respBody, err := ioutil.ReadAll(w.Body)

	if err != nil {
		t.Error(err)
	}

	if bytes.Compare(respBody, []byte(`{"message":"Hello test-request, Thanks for using api.mallon.io","status":"success"}`)) != 0 {
		t.Log(fmt.Sprintf("Got response: %s", respBody))
		t.Error("The response body is not what we expected to receive.")
	}

	if w.Code != http.StatusOK {
		t.Error("Expected to get a 200 response but got", w.Code)
	}
}
