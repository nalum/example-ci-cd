package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
)

var (
	hostFlag string
	portFlag int
)

const (
	serviceName = "API 2"
)

func init() {
	flag.StringVar(&hostFlag, "host", "0.0.0.0", "The host address to listen on.")
	flag.IntVar(&portFlag, "port", 8080, "The port to listen on.")
}

func main() {
	flag.Parse()
	r := router()
	log.Println(fmt.Sprintf("Starting %s Server on %v:%v", serviceName, hostFlag, portFlag))

	if err := r.Run(hostFlag + ":" + strconv.Itoa(portFlag)); err != nil {
		log.Fatal(err)
	}
}
