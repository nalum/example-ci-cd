package main

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAlive(t *testing.T) {
	req := httptest.NewRequest("GET", "https://api.mallon.io/alive", nil)
	w := httptest.NewRecorder()

	engine := router()
	engine.ServeHTTP(w, req)

	respBody, err := ioutil.ReadAll(w.Body)

	if err != nil {
		t.Error(err)
	}

	if bytes.Compare(respBody, []byte(`OK`)) != 0 {
		t.Error("The response body is not what we expected to receive.")
	}

	if w.Code != http.StatusOK {
		t.Error("Expected to get a 200 response but got", w.Code)
	}
}

func TestReady(t *testing.T) {
	req := httptest.NewRequest("GET", "https://api.mallon.io/ready", nil)
	w := httptest.NewRecorder()

	engine := router()
	engine.ServeHTTP(w, req)

	respBody, err := ioutil.ReadAll(w.Body)

	if err != nil {
		t.Error(err)
	}

	if bytes.Compare(respBody, []byte(`OK`)) != 0 {
		t.Error("The response body is not what we expected to receive.")
	}

	if w.Code != http.StatusOK {
		t.Error("Expected to get a 200 response but got", w.Code)
	}
}
