package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func getResponse(c *gin.Context) {
	c.JSON(200, map[string]string{
		"status":  "success",
		"message": fmt.Sprintf("Hello %s, Thanks for using api.mallon.io", c.Param("identifier")),
	})
}
