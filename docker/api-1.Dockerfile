FROM alpine:3.7

ARG APP_NAME
ARG VERSION
ARG COMMIT

ENV APP_NAME=${APP_NAME:-bin/service-example-ci-ci-api-1-development}
ENV VERSION=${VERSION:-development}
ENV COMMIT=${COMMIT}

COPY $APP_NAME /bin/app

EXPOSE 8080

ENTRYPOINT ["/bin/app"]

CMD ["-h"]
